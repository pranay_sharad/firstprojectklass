import { LightningElement, track } from 'lwc';
import getLoginUsers from '@salesforce/apex/loginPage.getLoginUsers'
export default class Header extends LightningElement {
    @track username;
    @track password;
    @track status;
    @track message;
    connectedCallback(){
        this.getLoginUsersApex(this.username)
    }
    getLoginUsersApex(usernamed){
        getLoginUsers({
            username:usernamed
        }).then((result)=>{
            console.log('Result',result)
        }).catch((err)=>{
            console.log('err',err)
        })
    }
    handleLogin(){
        // console.log('handleClick',this.username,this.password)
        let user = this.username;
        let pass = this.password;
        if(pass === '1234' && user === 'pranay'){
            // console.log('true',this.username);
            this.status = true;
            this.message = 'Welcome Pranay Rangne';
        }else{
            this.status = false;
            // console.log('false');
        }
    }
    handleChange(e){
        if(e.target.name === 'username'){
            this.username = e.target.value
        }
        if(e.target.name === 'password'){
            this.password = e.target.value
        }
    }
}