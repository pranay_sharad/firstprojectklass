public with sharing class loginPage {
    @AuraEnabled(cacheable=true)
   public static Contact getLoginUsers(Email mail){
       return [SELECT Id,Email FROM Contact WHERE Email :mail];
   }

}
